import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TTCtest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testZone1() {
		TTC emad = new TTC();
		String[] one = {"Leslie"};
		String[] two = {"Don Mills"};
		
		double data = emad.calculateTotal(one, two);
		System.out.println(data);
		assertEquals(2.50, data,0);
		
	}
	@Test
	public void testZone2() {
		TTC emad = new TTC();
		String[] one = {"Sheppard"};
		String[] two = {"Finch"};
		
		double data = emad.calculateTotal(one, two);
		System.out.println(data);
		assertEquals(3.00, data,0);
		
	}
	@Test
	public void testZones() {
		TTC emad = new TTC();
		String[] one = {"Don Mills"};
		String[] two = {"Finch"};
		
		double data = emad.calculateTotal(one, two);
		System.out.println(data);
		assertEquals(3.00, data,0);
		
	}
	@Test
	public void testgTwoZones() {
		TTC emad = new TTC();
		String[] one = {"Finch","Leslie"};
		String[] two = {"Sheppard","Don Mills"};
		
		double data = emad.calculateTotal(one, two);
		System.out.println(data);
		assertEquals(5.50, data,0);
		
	}
	@Test
	public void testgThreeZones() {
		TTC emad = new TTC();
		String[] one = {"Finch","Sheppard","Finch"};
		String[] two = {"Sheppard","Finch","Sheppard"};
		
		double data = emad.calculateTotal(one, two);
		System.out.println(data);
		assertEquals(6.0, data,0);
		
	}


}
